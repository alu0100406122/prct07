
Node = Struct.new(:value, :next) 


class Lista
    attr_reader :inicio
    attr_accessor :cabeza
    
    def initialize()
        @cabeza = nil
    end
    
    def insertar(x)
        node = Node.new(x,nil)
        if (@inicio != nil)  #Si la lista no está vacía...
            @cabeza.next = node
            @cabeza = node
        else                #Si la lista está vacía...
            @inicio = node
            @cabeza = @inicio
        end
    end
    
    def vacio()
        if (@inicio==nil)
            return true
        else
            return false
        end 
    end
    
    def borrar_inicio()
        if (@inicio != nil)
            aux = @inicio
            @inicio = aux.next
            return aux
        else
            return nil
        end
    end
    
    def insertar_array(*bloque)
        bloque.each do |node|
            if (@inicio != nil)  #Si la lista no está vacía...
                @cabeza.next = node
                @cabeza = node
            else                #Si la lista está vacía...
                @inicio = node
                @cabeza = @inicio
            end
        end
    end
    
    def borrar_final()
        if (@inicio != nil)
            aux = @inicio   #Me posiciono al principio de la lista
            aux1 = aux
            while (aux.next != nil) do    #Avanzo hasta el penúltimo elemento
                aux1 = aux
                aux = aux.next
            end
            if(aux.next == nil)
                if(aux == @inicio)
                   @inicio = nil 
                   @cabeza = @inicio
                else
                    @cabeza = aux1
                    aux1.next = nil
                end
            end
            return aux
        else
            return nil
        end
    end
    
    def to_s()
        aux = @inicio
        while (aux != nil) do
            "#{aux.value}"
            aux = aux.next
        end
        if (vacio() == true)
            #puts "La lista está vacía."
        end
    end
    
end