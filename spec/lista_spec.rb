require 'spec_helper'
require 'biblio'

describe Lista do
    before :each do
        #(autor, titulo, editorial, serie, edicion, publicacion, isbn)
        @libro1 = Bibliografia.new(["Dave Thomas", "Andy Hunt", "Chad Fowler"], "Programming Ruby 1.9 & 2.0: The Pragmatic Programmers’ Guide. (The Facets of Ruby)", "Pragmatic Bookshelf", "", 4, "(July 7, 2013)", ["ISBN-13: 978-1937785499", "ISBN-10: 1937785491"])
        @libro2 = Bibliografia.new(["Scott Chacon"], "Pro Git 2009th Edition.(Pro)", "Apress", "", 2009, "(August 27, 2009)", ["ISBN-13: 978-1430218333","ISBN-10: 1430218339"])
        @libro3 = Bibliografia.new(["David Flanagan","Yukihiro Matsumoto"],"The Ruby Programming Language.", "O’Reilly Media", "", 1, "(February 4, 2008)",["ISBN-10: 0596516177","ISBN-13: 978-0596516178"])
        @libro4 = Bibliografia.new(["David Chelimsky","Dave Astels","Bryan Helmkamp","Dan North","Zach Dennis","Aslak Hellesoy"],"The RSpec Book: Behaviour Driven Development with RSpec, Cucumber, and Friends (The Facets of Ruby).", "PragmaticBookshelf", "", 1, "(December 25, 2010)",["ISBN-10: 1934356379","ISBN-13: 978-1934356371"])
        @libro5 = Bibliografia.new(["Richard E"],"Silverman Git Pocket Guide", "O’Reilly Media", "", 1, "(August 2, 2013)",["ISBN-10: 1449325866","ISBN-13: 978-1449325862"])
    
        @nodo1 = Node.new(@libro1,nil)
        @nodo2 = Node.new(@libro2, nil)
        @nodo3 = Node.new(@libro3, nil)
        @nodo4 = Node.new(@libro4, nil)
        @nodo5 = Node.new(@libro5, nil)
        
        @lista1 = Lista.new()
    end
    
    describe "Node..." do
        
        it "El objeto pertenece a la clase..." do
            expect(@nodo1.is_a? Node).to be true
            expect(@nodo1.instance_of? Node).to be true
        end
        
        it "Debe existir un nodo de la Lista con sus datos y su siguiente..." do
            @lista1.insertar(@nodo1)
            expect(@lista1.vacio()).not_to be true
        end
        
        it "Debe existir un nodo con sus datos ..." do
            @lista1.insertar(@nodo1)
            expect(@lista1.borrar_inicio().value).not_to be nil
        end
        
        it "Debe existir un nodo con su siguiente..." do
            @lista1.insertar(@nodo1)
            @lista1.insertar(@nodo2)
            expect(@lista1.borrar_inicio().next).not_to be nil
        end
        
    end
    
    describe "List..." do
    
        it "El objeto pertenece a la clase..." do
            expect(@lista1.is_a? Lista).to be true
            expect(@lista1.instance_of? Lista).to be true
        end
        
        it "Se puede insertar un elemento..." do
            @lista1.insertar(@nodo1)
            expect(@lista1.vacio()).not_to be true
        end
        
        it "Se extrae el primer elemento de la lista..." do
            @lista1.insertar(@nodo2)
            @lista1.insertar(@nodo3)
            @lista1.borrar_inicio()
            expect(@lista1.borrar_inicio().value.to_s) == ("[Scott Chacon], Pro Git 2009th Edition.(Pro), Apress, , 2009, (August 27, 2009), [ISBN-13: 978-1430218333,ISBN-10: 1430218339]")
        end
        
        it "Se pueden insertar varios elementos..." do
            node = Array.new(3)
            node[0] = @nodo1
            node[1] = @nodo2
            node[2] = @nodo3
            @lista1.insertar_array(node)
            expect(@lista1.vacio()).not_to be true
        end
        
        it "Debe existir una lista con su cabeza..." do
            @lista1.insertar(@nodo1)
            expect(@lista1.borrar_final()).not_to be nil
        end
        
    end
    
    
    describe "Mostrar libros..." do
        
        it "Mostrar un libro..." do
            expect(@libro1.to_s) == (["Dave Thomas, Andy Hunt, Chad Fowler], Programming Ruby 1.9 & 2.0: The Pragmatic Programmers’ Guide. (The Facets of Ruby), Pragmatic Bookshelf, , 4, (July 7, 2013), [ISBN-13: 978-1937785499, ISBN-10: 1937785491"])
        end
        
        it "Mostrar varios libros..." do
            @lista1.insertar(@nodo1)
            @lista1.insertar(@nodo2)
            @lista1.insertar(@nodo3)
            @lista1.insertar(@nodo4)
            @lista1.insertar(@nodo5)
            expect (@lista1.to_s) == ("#{@nodo1.value}, #{@nodo2.value}, #{@nodo3.value}, #{@nodo4.value}, #{@nodo5.value}") 
        end
        
    end
    
end